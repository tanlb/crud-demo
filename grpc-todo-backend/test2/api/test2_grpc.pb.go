// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v5.27.0
// source: test2/api/test2.proto

package test2

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// Test2Client is the client API for Test2 service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type Test2Client interface {
	Test2Method(ctx context.Context, in *Test2Request, opts ...grpc.CallOption) (*Test2Response, error)
}

type test2Client struct {
	cc grpc.ClientConnInterface
}

func NewTest2Client(cc grpc.ClientConnInterface) Test2Client {
	return &test2Client{cc}
}

func (c *test2Client) Test2Method(ctx context.Context, in *Test2Request, opts ...grpc.CallOption) (*Test2Response, error) {
	out := new(Test2Response)
	err := c.cc.Invoke(ctx, "/test2.Test2/Test2Method", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Test2Server is the server API for Test2 service.
// All implementations must embed UnimplementedTest2Server
// for forward compatibility
type Test2Server interface {
	Test2Method(context.Context, *Test2Request) (*Test2Response, error)
	mustEmbedUnimplementedTest2Server()
}

// UnimplementedTest2Server must be embedded to have forward compatible implementations.
type UnimplementedTest2Server struct {
}

func (UnimplementedTest2Server) Test2Method(context.Context, *Test2Request) (*Test2Response, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Test2Method not implemented")
}
func (UnimplementedTest2Server) mustEmbedUnimplementedTest2Server() {}

// UnsafeTest2Server may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to Test2Server will
// result in compilation errors.
type UnsafeTest2Server interface {
	mustEmbedUnimplementedTest2Server()
}

func RegisterTest2Server(s grpc.ServiceRegistrar, srv Test2Server) {
	s.RegisterService(&Test2_ServiceDesc, srv)
}

func _Test2_Test2Method_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Test2Request)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(Test2Server).Test2Method(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/test2.Test2/Test2Method",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(Test2Server).Test2Method(ctx, req.(*Test2Request))
	}
	return interceptor(ctx, in, info, handler)
}

// Test2_ServiceDesc is the grpc.ServiceDesc for Test2 service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Test2_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "test2.Test2",
	HandlerType: (*Test2Server)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Test2Method",
			Handler:    _Test2_Test2Method_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "test2/api/test2.proto",
}
