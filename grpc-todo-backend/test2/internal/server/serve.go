package server

import (
	"context"

	mykit "gitlab.ugaming.io/marketplace/mykit/pkg/api"
	"go.uber.org/zap"
	"google.golang.org/grpc/reflection"

	pb0 "test2/api"
	"test2/internal/server/crud"
	"test2/internal/server/test2"
	config "test2/pkg/config"

	entcli "test2/pkg/ent"

	dbe "gitlab.ugaming.io/marketplace/database/pkg/ent"
)

// Serve ...
func Serve(cfg *config.Config) {
	service := newService(cfg, []mykit.Option{}...)

	drv, err := dbe.Open("mysql_test", cfg.GetDatabase())
	if err != nil {
		service.Logger().Fatal("could not connect to db", zap.Error(err))
	}

	ent := entcli.NewClient(entcli.Driver(drv))

	defer func() {
		if err := ent.Close(); err != nil {
			service.Logger().Fatal("could not close ent client", zap.Error(err))
		}
	}()

	if err = ent.Schema.Create(context.Background()); err != nil {
		service.Logger().Fatal("could not init database", zap.Error(err))
	}

	server := service.Server()
	pb0.RegisterTest2Server(server, test2.NewServer(ent))
	pb0.RegisterCRUDServer(server, crud.NewServer(ent))

	// Register reflection service on gRPC server.
	// Please remove if you it's not necessary for your service
	reflection.Register(server)

	service.Serve()
}
