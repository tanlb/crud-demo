package transformer

import (
	pb "test2/api"
	"test2/pkg/ent"
)

func TodoPB_To_TodoModel(product *pb.Todo) (*ent.Todo, error) {
	return &ent.Todo{
		ID:          product.GetId(),
		Title:       product.GetTitle(),
		Description: product.GetDescription(),
		Completed:   product.GetCompleted(),
	}, nil
}

func TodoModel_To_TodoPB(product *ent.Todo) (*pb.Todo, error) {
	return &pb.Todo{
		Id:          product.ID,
		Title:       product.Title,
		Description: product.Description,
		Completed:   product.Completed,
	}, nil
}

func TodoListModel_To_TodoListPB(products []*ent.Todo) (*pb.Todos, error) {
	var result []*pb.Todo
	for _, product := range products {
		model, err := TodoModel_To_TodoPB(product)
		if err != nil {
			return nil, err
		}
		result = append(result, model)
	}
	response := &pb.Todos{
		Todos: result,
	}
	return response, nil
}
