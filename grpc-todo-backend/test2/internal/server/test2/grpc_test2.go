package test2

import (
	test2 "test2/api"
	"test2/pkg/ent"
)

func NewServer(ent *ent.Client) test2.Test2Server {
	return &test2Server{
		entClient: ent,
	}
}

type test2Server struct {
	test2.UnimplementedTest2Server
	entClient *ent.Client
}
