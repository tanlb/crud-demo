package test2

import (
	"context"

	"test2/api"
)

func (s *test2Server) Test2Method(ctx context.Context, request *test2.Test2Request) (*test2.Test2Response, error) {
	if err := request.Validate(); err != nil {
		return nil, err
	}

	return &test2.Test2Response{}, nil
}
