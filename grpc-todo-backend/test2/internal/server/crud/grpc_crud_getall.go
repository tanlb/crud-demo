package crud

import (
	"context"

	test2 "test2/api"
	"test2/internal/server/transformer"
	"test2/pkg/ent"
)

func (s *cRUDServer) GetAll(ctx context.Context, request *test2.Empty) (*test2.Todos, error) {
	if err := request.Validate(); err != nil {
		return nil, err
	}

	todos, err := s.entClinet.Todo.Query().
		Order(ent.Asc("title")).
		All(ctx)

	if err != nil {
		return nil, err
	}
	todosPB, errorTransform := transformer.TodoListModel_To_TodoListPB(todos)

	if errorTransform != nil {
		return nil, errorTransform
	}

	return &test2.Todos{
		Todos: todosPB.Todos,
	}, nil
}
