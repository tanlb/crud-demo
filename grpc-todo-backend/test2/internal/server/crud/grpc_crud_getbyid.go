package crud

import (
	"context"

	test2 "test2/api"
	"test2/internal/server/transformer"
	"test2/pkg/ent/todo"
)

func (s *cRUDServer) GetByID(ctx context.Context, request *test2.ID) (*test2.Todo, error) {
	if err := request.Validate(); err != nil {
		return nil, err
	}

	todo, err := s.entClinet.Todo.Query().
		Where(todo.ID(request.GetId())).
		Only(ctx)

	if err != nil {
		return nil, err
	}

	todoBP, errorTransform := transformer.TodoModel_To_TodoPB(todo)

	if errorTransform != nil {
		return nil, errorTransform
	}

	return todoBP, nil

	// return &test2.Todo{}, nil
}
