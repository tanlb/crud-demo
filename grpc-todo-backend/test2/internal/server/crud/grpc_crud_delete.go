package crud

import (
	"context"

	test2 "test2/api"
)

func (s *cRUDServer) Delete(ctx context.Context, request *test2.ID) (*test2.Empty, error) {
	if err := request.Validate(); err != nil {
		return nil, err
	}

	err := s.entClinet.Todo.DeleteOneID(request.GetId()).Exec(ctx)

	if err != nil {
		return nil, err
	}

	return &test2.Empty{}, nil
}
