package crud

import (
	"context"

	test2 "test2/api"
	"test2/internal/server/transformer"
)

func (s *cRUDServer) Update(ctx context.Context, request *test2.Todo) (*test2.Todo, error) {
	if err := request.Validate(); err != nil {
		return nil, err
	}

	todo, err := s.entClinet.Todo.
		UpdateOneID(request.GetId()).
		SetTitle(request.GetTitle()).
		SetDescription(request.GetDescription()).
		SetCompleted(request.GetCompleted()).
		Save(ctx)

	if err != nil {
		return nil, err
	}

	todoBP, errorTransform := transformer.TodoModel_To_TodoPB(todo)

	if errorTransform != nil {
		return nil, errorTransform
	}

	return todoBP, nil

	// return &test2.Todo{}, nil
}
