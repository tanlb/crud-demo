package crud

import (
	test2 "test2/api"
	"test2/pkg/ent"
)

func NewServer(ent *ent.Client) test2.CRUDServer {
	return &cRUDServer{
		entClinet: ent,
	}
}

type cRUDServer struct {
	test2.UnimplementedCRUDServer
	entClinet *ent.Client
}
