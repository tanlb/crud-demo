package crud

import (
	"context"
	"fmt"

	test2 "test2/api"
	"test2/internal/server/transformer"
)

func (s *cRUDServer) Create(ctx context.Context, request *test2.Todo) (*test2.Todo, error) {
	if err := request.Validate(); err != nil {
		return nil, err
	}

	fmt.Println("Create Todo")

	newTodo, err := s.entClinet.Todo.Create().
		SetID(request.GetId()).
		SetTitle(request.GetTitle()).
		SetDescription(request.GetDescription()).
		SetCompleted(request.GetCompleted()).
		Save(ctx)

	if err != nil {
		return nil, err
	}

	newTodoPB, errTransform := transformer.TodoModel_To_TodoPB(newTodo)
	if errTransform != nil {
		return nil, errTransform
	}

	return newTodoPB, nil

	// return &test2.Todo{}, nil
}
