const Home = () => {

    return(
        <>
        <div className="text-center">
            <h2>Find a movie to watch tonight!</h2>
            <hr />
        </div>
        </>
    )
}

export default Home;