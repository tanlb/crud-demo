
import { useEffect, useState } from 'react';
import { CRUDClient } from './../../todo_grpc_web_pb';
import { Empty, ID, Todo } from './../../todo_pb';

const client = new CRUDClient('http://localhost:8080');

let newId = 0;

function ListTodoComponent() {


    const [todos, setTodos] = useState([]);
    const [editTodos, setEditTodos] = useState({});



    const [newTitle, setNewTitle] = useState("");
    const [newDescription, setNewDescription] = useState("");
    const [newCompleted, setNewCompleted] = useState(false);

    useEffect(() => {

        refreshTodos();
    }, []);

    






    function refreshTodos() {
        // await getAllTodos();
        client.getAll(new Empty(), null, (err, response) => {
            if (err) {
                console.error(err);
                return;
            }

            let todos = response.getTodosList();

            setTodos(todos);

            setEditTodos(todos.reduce((acc, todo) => {

                acc[todo.getId()] = {
                    title: todo.getTitle(),
                    description: todo.getDescription(),
                    completed: todo.getCompleted()
                };
                return acc;
            }, {}));

        });


        // setEditTodos(todos.reduce((acc, todo) => {

        //     acc[todo.getId()] = {
        //         title: todo.getTitle(),
        //         description: todo.getDescription(),
        //         completed: todo.getCompleted()
        //     };
        //     return acc;
        // }, {}));

        setNewTitle("");
        setNewDescription("");
        setNewCompleted(false);
    }

    function deleteTodo(id) {
        let deleteID = new ID();
        deleteID.setId(id);
        client.delete(deleteID, {}, (err, response) => {
            if (err) {
                console.error(err);
                return;
            }
            refreshTodos();
        });
    }

    function handleInputChange(id, field, value) {
        setEditTodos({
            ...editTodos,
            [id]: {
                ...editTodos[id],
                [field]: value
            }
        });
    }

    function updateTodo(id) {
        // let updateID = new ID();
        // updateID.setId(id);

        // client.getByID(updateID, {}, (err, response) => {
        //     if (err) {
        //         console.error(err);
        //         return;
        //     }

        //     let todo = response;
        //     let newTodo = new Todo();
        //     newTodo.setId(todo.getId());
        //     newTodo.setTitle(todo.getTitle());
        //     newTodo.setDescription(todo.getDescription());
        //     newTodo.setCompleted(todo.getCompleted());
        //     client.update(newTodo, {}, (err, response) => {
        //         if (err) {
        //             console.error(err);
        //             return;
        //         }
        //         refreshTodos();
        //     });
        // })
        const todoToUpdate = new Todo();
        todoToUpdate.setId(id);
        todoToUpdate.setTitle(editTodos[id].title);
        todoToUpdate.setDescription(editTodos[id].description);
        todoToUpdate.setCompleted(editTodos[id].completed);

        client.update(todoToUpdate, null, (err, response) => {
            if (err) {
                console.error(err);
                return;
            }
            refreshTodos();
        });

    }

    function addNewTodo() {
    if (newTitle === "" || newDescription === "") {
        return;
    } else {
        newId++;
        let newTodo = new Todo();
        newTodo.setTitle(newTitle);
        newTodo.setDescription(newDescription);
        newTodo.setCompleted(newCompleted);
        newTodo.setId(newId.toString());
        client.create(newTodo, null, (err, response) => {
            if (err) {
                console.error(err);
                return;
            }

            refreshTodos();

        });
    }
        

    }


    return (
        <div className="ListTodo container-fluid p-5">
            <h1 className='mt-5'>Things You Want To Do!</h1>

            <div className='p-5'>
                <table className="table table-hover table-bordered table_custom">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Is Done?</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            todos.map(todo => {
                                const todoId = todo.getId();
                                const editTodo = editTodos[todoId] || { title: '', description: '', completed: false }; // Add a default fallback
                                return (
                                    <tr key={todoId}>
                                        <td><input type="text"
                                            value={editTodo.title}
                                            onChange={(e) => handleInputChange(todoId, 'title', e.target.value)}
                                        /></td>
                                        <td><input type="text"
                                            value={editTodo.description}
                                            onChange={(e) => handleInputChange(todoId, 'description', e.target.value)}
                                        /></td>
                                        <td><select
                                            aria-label="Filter"
                                            value={editTodo.completed.toString()}
                                            onChange={(e) => handleInputChange(todoId, 'completed', e.target.value === 'true')}
                                        >
                                            <option value="false">In progress</option>
                                            <option value="true">Completed</option>
                                        </select></td>
                                        <td> <button className="btn btn-outline-warning"
                                            onClick={() => deleteTodo(todoId)}>Delete</button> </td>
                                        <td> <button className="btn btn-outline-primary"
                                            onClick={() => updateTodo(todoId)}>Update</button> </td>
                                    </tr>
                                );
                            })
                        }

                        <tr>
                            <td><input type="text" value={newTitle} onChange={(e) => {
                                setNewTitle(e.target.value)
                            }} /></td>
                            <td><input type="text" value={newDescription} onChange={(e) => {
                                setNewDescription(e.target.value)
                            }} /></td>
                            <td><select
                                aria-label="Filter"
                                value={newCompleted.toString()}
                                onChange={(e) => {
                                    setNewCompleted(e.target.value)
                                }}
                            >
                                <option value="false">In progress</option>
                                <option value="true">Completed</option>
                            </select></td>
                            <td> </td>
                            <td> </td>
                        </tr>
                    </tbody>

                </table>
            </div>
            <div className="btn btn-primary m-1 btn-lg" onClick={addNewTodo}>Add New Todo</div>
        </div>
    );
}

export default ListTodoComponent;
