/**
 * @fileoverview gRPC-Web generated client stub for test2
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.test2 = require('./todo_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.test2.CRUDClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.test2.CRUDPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.test2.Empty,
 *   !proto.test2.Todos>}
 */
const methodDescriptor_CRUD_GetAll = new grpc.web.MethodDescriptor(
  '/test2.CRUD/GetAll',
  grpc.web.MethodType.UNARY,
  proto.test2.Empty,
  proto.test2.Todos,
  /**
   * @param {!proto.test2.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.test2.Todos.deserializeBinary
);


/**
 * @param {!proto.test2.Empty} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.test2.Todos)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.test2.Todos>|undefined}
 *     The XHR Node Readable Stream
 */
proto.test2.CRUDClient.prototype.getAll =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/test2.CRUD/GetAll',
      request,
      metadata || {},
      methodDescriptor_CRUD_GetAll,
      callback);
};


/**
 * @param {!proto.test2.Empty} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.test2.Todos>}
 *     Promise that resolves to the response
 */
proto.test2.CRUDPromiseClient.prototype.getAll =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/test2.CRUD/GetAll',
      request,
      metadata || {},
      methodDescriptor_CRUD_GetAll);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.test2.ID,
 *   !proto.test2.Todo>}
 */
const methodDescriptor_CRUD_GetByID = new grpc.web.MethodDescriptor(
  '/test2.CRUD/GetByID',
  grpc.web.MethodType.UNARY,
  proto.test2.ID,
  proto.test2.Todo,
  /**
   * @param {!proto.test2.ID} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.test2.Todo.deserializeBinary
);


/**
 * @param {!proto.test2.ID} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.test2.Todo)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.test2.Todo>|undefined}
 *     The XHR Node Readable Stream
 */
proto.test2.CRUDClient.prototype.getByID =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/test2.CRUD/GetByID',
      request,
      metadata || {},
      methodDescriptor_CRUD_GetByID,
      callback);
};


/**
 * @param {!proto.test2.ID} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.test2.Todo>}
 *     Promise that resolves to the response
 */
proto.test2.CRUDPromiseClient.prototype.getByID =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/test2.CRUD/GetByID',
      request,
      metadata || {},
      methodDescriptor_CRUD_GetByID);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.test2.Todo,
 *   !proto.test2.Todo>}
 */
const methodDescriptor_CRUD_Create = new grpc.web.MethodDescriptor(
  '/test2.CRUD/Create',
  grpc.web.MethodType.UNARY,
  proto.test2.Todo,
  proto.test2.Todo,
  /**
   * @param {!proto.test2.Todo} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.test2.Todo.deserializeBinary
);


/**
 * @param {!proto.test2.Todo} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.test2.Todo)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.test2.Todo>|undefined}
 *     The XHR Node Readable Stream
 */
proto.test2.CRUDClient.prototype.create =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/test2.CRUD/Create',
      request,
      metadata || {},
      methodDescriptor_CRUD_Create,
      callback);
};


/**
 * @param {!proto.test2.Todo} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.test2.Todo>}
 *     Promise that resolves to the response
 */
proto.test2.CRUDPromiseClient.prototype.create =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/test2.CRUD/Create',
      request,
      metadata || {},
      methodDescriptor_CRUD_Create);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.test2.Todo,
 *   !proto.test2.Todo>}
 */
const methodDescriptor_CRUD_Update = new grpc.web.MethodDescriptor(
  '/test2.CRUD/Update',
  grpc.web.MethodType.UNARY,
  proto.test2.Todo,
  proto.test2.Todo,
  /**
   * @param {!proto.test2.Todo} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.test2.Todo.deserializeBinary
);


/**
 * @param {!proto.test2.Todo} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.test2.Todo)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.test2.Todo>|undefined}
 *     The XHR Node Readable Stream
 */
proto.test2.CRUDClient.prototype.update =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/test2.CRUD/Update',
      request,
      metadata || {},
      methodDescriptor_CRUD_Update,
      callback);
};


/**
 * @param {!proto.test2.Todo} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.test2.Todo>}
 *     Promise that resolves to the response
 */
proto.test2.CRUDPromiseClient.prototype.update =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/test2.CRUD/Update',
      request,
      metadata || {},
      methodDescriptor_CRUD_Update);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.test2.ID,
 *   !proto.test2.Empty>}
 */
const methodDescriptor_CRUD_Delete = new grpc.web.MethodDescriptor(
  '/test2.CRUD/Delete',
  grpc.web.MethodType.UNARY,
  proto.test2.ID,
  proto.test2.Empty,
  /**
   * @param {!proto.test2.ID} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.test2.Empty.deserializeBinary
);


/**
 * @param {!proto.test2.ID} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.test2.Empty)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.test2.Empty>|undefined}
 *     The XHR Node Readable Stream
 */
proto.test2.CRUDClient.prototype.delete =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/test2.CRUD/Delete',
      request,
      metadata || {},
      methodDescriptor_CRUD_Delete,
      callback);
};


/**
 * @param {!proto.test2.ID} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.test2.Empty>}
 *     Promise that resolves to the response
 */
proto.test2.CRUDPromiseClient.prototype.delete =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/test2.CRUD/Delete',
      request,
      metadata || {},
      methodDescriptor_CRUD_Delete);
};


module.exports = proto.test2;

