import './App.css';
import { CRUDClient } from './todo_grpc_web_pb';
import ListTodoComponent from './Components/list-todo/ListTodoComp';
import { Outlet } from 'react-router-dom';

const client = new CRUDClient('http://localhost:8080');

function App() {



  return (
    <div className="App">
      <>
        <ListTodoComponent />
        <Outlet context={{
            client
          }}/>
      </>
      
    </div>
  );
}

export default App;
